#ifndef WORD_ITEM_H_SENTRY
#define WORD_ITEM_H_SENTRY
#include "delimiter_tokens.h"

enum word_item_type_enum {
    TYPE_WORD,
    TYPE_DELIM
};

union word_item_type_union {
    char *word;
    enum delimeter_tokens tok;
};


struct tag_word_item {
    enum word_item_type_enum type;

    union {
        char *word;
        enum delimeter_tokens tok;
    };

    struct tag_word_item *next;
    struct tag_word_item *prev;
};

typedef struct tag_word_item word_item;

void word_item_print(word_item *ll);

void word_item_add(
    word_item **ll,
    union word_item_type_union data,
    enum word_item_type_enum type
);

void word_item_free(word_item *ll);

int word_item_len(const word_item *ll);

char **word_item_to_ntarr(const word_item *ll);

word_item* word_item_init();

#endif
