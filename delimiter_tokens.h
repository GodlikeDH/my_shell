#ifndef DELIMETER_TOKENS_H_SENTRY
#define DELIMETER_TOKENS_H_SENTRY
enum delimeter_tokens {
    NOT_A_DELIM_TOKEN,
    TOK_BACKGROUND_FLG,
    TOK_RDRCT_STDIN,
    TOK_RDRCT_STDOUT_F_APPND,
    TOK_RDRCT_STDOUT_F_TRUNC,
    TOK_EMPTY,
    TOK_PIPELINE,
};


enum delimeter_tokens str_tok_to_enum(const char* token);

char *enum_tok_to_str(enum delimeter_tokens tok);
#endif

