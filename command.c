#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>

#include "command.h"
#include "my_errors.h"
#include "delimiter_tokens.h"

static int redir_stdin_file(command *ret, const char *filename)
{
    if(str_tok_to_enum(filename) != NOT_A_DELIM_TOKEN) {
        return ERR_CMND_RDRCT_INPUT_BAD_FILENAME;
    }

    if(ret->input_fd != 0 || ret->pipe_input != 0) {
        return ERR_CMND_RDRCT_INPUT_TWICE;
    }

    int fd;
    fd = open(filename, O_RDONLY);
    if(fd == -1) {
        return -1;
    }

    ret->input_fd = fd;

    return SUCCESS;
}

static int redir_stdout_file(command *ret, const char *filename, int trunc)
{
    if(str_tok_to_enum(filename) != NOT_A_DELIM_TOKEN) {
        return ERR_CMND_RDRCT_OUTPUT_BAD_FILENAME;
    }

    if(ret->output_fd != 1 || ret->pipe_output != 0) {
        return ERR_CMND_RDRCT_OUTPUT_TWICE;
    }

    int fd;
    int mode;
    mode = trunc ? O_TRUNC : O_APPEND;
    fd = open(filename, O_WRONLY | O_CREAT | mode, 0666);
    if(fd == -1) {
        return -1;
    }

    ret->output_fd = fd;

    return SUCCESS;
}

static int validate_command(
        word_item *prev_item,
        word_item *next_item,
        enum delimeter_tokens tok
    )
{
    if(!prev_item && tok != NOT_A_DELIM_TOKEN) {
        return ERR_CMND_START_WITH_DELIM;
    }
    if(!next_item && tok == TOK_RDRCT_STDIN) {
        return ERR_CMND_RDRCT_INPUT_BAD_FILENAME;
    }
    if(!next_item && tok == TOK_RDRCT_STDOUT_F_APPND) {
        return ERR_CMND_RDRCT_OUTPUT_BAD_FILENAME;
    }
    if(!next_item && tok == TOK_RDRCT_STDOUT_F_TRUNC) {
        return ERR_CMND_RDRCT_OUTPUT_BAD_FILENAME;
    }
    if(next_item && tok == TOK_BACKGROUND_FLG) {
        return ERR_CMND_BAD_BACKGRND_TOK;
    }
    if(tok == TOK_PIPELINE && (!next_item || !prev_item)) {
        return ERR_CMND_BAD_PIPELINE;
    }
    if(tok != NOT_A_DELIM_TOKEN && prev_item && next_item &&
       (prev_item->type == TYPE_DELIM || next_item->type == TYPE_DELIM)
    ) {
        return ERR_CMND_TWO_DELIMS;
    }
    return SUCCESS;
}

static void command_arr_add(commands_arr *cmd_arr, command *cmd)
{
    if(cmd_arr->capacity == 0) {
        cmd_arr->commands = malloc(
            sizeof(command) * DEFAULT_COMMANDS_ARR_SIZE
        );
        cmd_arr->capacity = DEFAULT_COMMANDS_ARR_SIZE;
    }

    if(cmd_arr->capacity == cmd_arr->size) {
        cmd_arr->commands = realloc(
            cmd_arr->commands,
            sizeof(command) * DEFAULT_COMMANDS_ARR_SIZE * 2
        );
        cmd_arr->capacity *= 2;
    }

    cmd_arr->commands[cmd_arr->size] = *cmd;
    cmd_arr->size++;
}

void commands_arr_free(commands_arr *cmd_arr)
{
    for(int i = 0; i < cmd_arr->size; i++) {
        free(cmd_arr->commands[i].argv);
    }
    free(cmd_arr->commands);
}

static int create_command(
    const word_item *tokens,
    const word_item *ll,
    command *res,
    int *pipe_input_flag,
    int *pipe_output_flag
)
{
    int argv_size = 0;
    const word_item *tmp = tokens;

    /* set pipe flags before redirects to get proper errors */
    res->pipe_input = *pipe_input_flag;
    *pipe_input_flag = 0;
    res->pipe_output = *pipe_output_flag;
    *pipe_output_flag = 0;

    res->input_fd = 0;
    res->output_fd = 1;
    res->pid = 0;

    while(tmp != ll) { /* use cycle to found double reidrects */
        if(tmp->type == TYPE_DELIM) {
            int rc;
            rc = validate_command(tmp->prev, tmp->next, tmp->tok);
            if(rc != SUCCESS) {
                return rc;
            }
            if(tmp->tok == TOK_RDRCT_STDIN) {
                rc = redir_stdin_file(res, tmp->next->word);
            }
            if(tmp->tok == TOK_RDRCT_STDOUT_F_APPND ||
               tmp->tok == TOK_RDRCT_STDOUT_F_TRUNC) {
                rc = redir_stdout_file(
                    res,
                    tmp->next->word,
                    tmp->tok == TOK_RDRCT_STDOUT_F_TRUNC
                );
            }
            if(rc != SUCCESS) {
                return rc;
            }
            tmp = tmp->next->next;
        } else {
            tmp = tmp->next;
            argv_size++;
        }
    }

    res->argv = malloc(sizeof(char *) * (argv_size+1));
    int i = 0;
    while(tokens != ll) {
        if(tokens->type == TYPE_DELIM) {
            tokens = tokens->next->next;
        } else {
            res->argv[i] = tokens->word;
            tokens = tokens->next;
            i++;
        }
    }
    res->argv[argv_size] = NULL;

    return SUCCESS;
}

static int is_command_delimeter(enum delimeter_tokens tok)
{
    switch(tok) {
    case TOK_RDRCT_STDIN:
    case TOK_RDRCT_STDOUT_F_APPND:
    case TOK_RDRCT_STDOUT_F_TRUNC:
        return 0;
    default:
        return 1;
    }
}

int parse_commands(const word_item *tokens, commands_arr *cmd_arr)
{
    commands_arr result_arr = {0};
    const word_item *ll = tokens;
    int rc;
    command cmd = {0};

    int pipe_input_flag = 0;
    int pipe_output_flag = 0;

    while(ll) {
        if(ll->type == TYPE_WORD || !is_command_delimeter(ll->tok)) {
            ll = ll->next;
            continue;
        }

        rc = validate_command(ll->prev, ll->next, ll->tok);
        if(rc != SUCCESS) {
            return rc;
        }

        if(ll->tok == TOK_PIPELINE) {
            pipe_output_flag = 1;
        }
        rc = create_command(
            tokens, ll, &cmd,
            &pipe_input_flag, &pipe_output_flag
        );
        if(rc != SUCCESS) {
            return rc;
        }
        command_arr_add(&result_arr, &cmd);
        if(ll->tok == TOK_PIPELINE) {
            pipe_input_flag = 1;
        }

        if(ll->tok == TOK_BACKGROUND_FLG) {
            result_arr.is_background = 1;
        }

        ll = ll->next;
        tokens = ll;
    }

    if(ll != tokens) {
        rc = create_command(
            tokens, ll, &cmd,
            &pipe_input_flag, &pipe_output_flag
        );
        if(rc != SUCCESS) {
            return rc;
        }
        command_arr_add(&result_arr, &cmd);
    }

    result_arr.commands[result_arr.size-1].pipe_output = 0;
    if(pipe_input_flag) {
        result_arr.commands[result_arr.size-1].pipe_input = 1;
    }

    *cmd_arr = result_arr;
    return SUCCESS;
}

void print_command(command *cmnd)
{
    char **argv;
    argv = cmnd->argv;
    printf("command: ");
    while(*argv) {
        printf("%s ", *argv);
        argv++;
    }
    printf("in_pipe: %d out_pipe: %d\n", cmnd->pipe_input, cmnd->pipe_output);
}

void print_commands(commands_arr *cmnd_arr)
{
    for(int i = 0; i < cmnd_arr->size; i++) {
        print_command(&cmnd_arr->commands[i]);
    }
    printf("\n");
}
