#include <string.h>

#include "delimiter_tokens.h"

enum delimeter_tokens str_tok_to_enum(const char* token)
{
    if(strcmp(token, "&") == 0) {
        return TOK_BACKGROUND_FLG;
    }
    if(strcmp(token, ">") == 0) {
        return TOK_RDRCT_STDOUT_F_TRUNC;
    }
    if(strcmp(token, ">>") == 0) {
        return TOK_RDRCT_STDOUT_F_APPND;
    }
    if(strcmp(token, "<") == 0) {
        return TOK_RDRCT_STDIN;
    }
    if(strcmp(token, "") == 0) {
        return TOK_EMPTY;
    }
    if(strcmp(token, "|") == 0) {
        return TOK_PIPELINE;
    }
    return NOT_A_DELIM_TOKEN;
}

char *enum_tok_to_str(enum delimeter_tokens tok) {
    switch(tok) {
    case TOK_BACKGROUND_FLG:
        return "Token: &";
    case TOK_RDRCT_STDIN:
        return "Token: <";
    case TOK_RDRCT_STDOUT_F_TRUNC:
        return "Token: >";
    case TOK_RDRCT_STDOUT_F_APPND:
        return "Token: >>";
    case TOK_EMPTY:
        return "Token: EMPTY_TOKEN";
    case TOK_PIPELINE:
        return "Token: |";
    default:
        return "Something is wrong";
    }
}
