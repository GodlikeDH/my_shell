#include "word_item.h"

struct tag_command {
    char **argv;
    int input_fd;
    int output_fd;
    int pipe_input;
    int pipe_output;
    int pid;
};
typedef struct tag_command command;

struct tag_commands_arr {
    command *commands;
    int size;
    int capacity;
    int is_background;
};
typedef struct tag_commands_arr commands_arr;

enum {DEFAULT_COMMANDS_ARR_SIZE = 100};

int parse_commands(const word_item *tokens, commands_arr *cmd_arr);
void print_command(command *cmnd);
void print_commands(commands_arr *cmnd_arr);
void commands_arr_free(commands_arr *cmd_arr);
