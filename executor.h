#ifndef EXECUTOR_H_SENTRY
#define EXECUTOR_H_SENTRY

#include "word_item.h"

int execute(const word_item *tokens);

#endif
