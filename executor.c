#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/wait.h>
#include <fcntl.h>

#include "executor.h"
#include "my_errors.h"
#include "command.h"
#include "utils.h"

static int change_dir(const char *path)
{
    int rc;
    rc = chdir(path);
    return rc;
}

static int execute_cd(const word_item *tokens)
{
    switch(word_item_len(tokens)) {
        case 2:
            return change_dir(tokens->next->word);
        case 1:
        {
            char *home_dir;
            home_dir = getenv("HOME");
            if(!home_dir) {
                return ERR_CD_NO_HOME;
            }
            return change_dir(home_dir);
        }
        default:
            return ERR_CD_TOO_MANY_ARGS;
    }
}

int execute(const word_item *tokens)
{
    if(strcmp(tokens->word, "cd") == 0) {
        return execute_cd(tokens);
    }

    int rc;
    commands_arr cmd_arr;

    rc = parse_commands(tokens, &cmd_arr);
    if(rc != SUCCESS) {
        return rc;
    }

    int fds[2];
    fds[0] = -1;
    fds[1] = -1;

    int prev_fds[2];
    prev_fds[0] = -1;
    prev_fds[1] = -1;

    for(int i = 0; i < cmd_arr.size; i++) {
        command *cmd = &cmd_arr.commands[i];

       if(cmd->pipe_input) {
            safe_close(prev_fds[0]);
            prev_fds[0] = fds[0];
            prev_fds[1] = fds[1];
            cmd->input_fd = prev_fds[0];
        }

        if(cmd->pipe_output) {
            pipe(fds);
            cmd->output_fd = fds[1];
            safe_close(prev_fds[1]);
        }

        int pid = fork();
        cmd->pid = pid;

        if(pid == 0) {
            /* child process */

            if(cmd->input_fd != 0) {
                dup2(cmd->input_fd, 0);
            }

            if(cmd->output_fd != 1) {
                dup2(cmd->output_fd, 1);
                safe_close(fds[0]);
            }

            if(cmd->pipe_output == 0) {
                /* last pipeline element */
                safe_close(fds[1]);
            }


            execvp(cmd->argv[0], cmd->argv);
            perror(cmd->argv[0]);
            fflush(stderr);
            _exit(1);

        }
    }
    safe_close(fds[0]);
    safe_close(fds[1]);


    int status;
    pid_t wpid;

    if(!cmd_arr.is_background) {
        for(int i = 0; i < cmd_arr.size; i++) {
            int pid = cmd_arr.commands[i].pid;
            do {
                wpid = wait(&status);
                print_exit_status(status, wpid);
            } while(wpid != pid && wpid > 0);
        }

        commands_arr_free(&cmd_arr);

        return SUCCESS;

    }

    commands_arr_free(&cmd_arr);

    return SUCCESS;
}

