CC = gcc
CFLAGS = -g -Wall

SRCMODULES = \
	word_item.c tokenizer.c my_errors.c \
	executor.c command.c delimiter_tokens.c \
	signals.c utils.c

OBJMODULES = $(SRCMODULES:.c=.o)

all: my_shell

debug: CFLAGS += -DDEBUG
debug: my_shell

%.o: %.c %.h
	$(CC) $(CFLAGS) -c $< -o $@
my_shell: my_shell.c $(OBJMODULES)
	$(CC) $(CFLAGS) $^ -o $@
clean:
	-rm $(OBJMODULES) my_shell
