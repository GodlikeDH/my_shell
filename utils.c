#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "utils.h"

int safe_close(int fd) {
    int res = 0;
    if(fd != -1) {
        return close(fd);
    }
    return res;
}

void print_exit_status(int status, int pid) {
    if(pid <= 0) {
        return;
    }

    if(WIFEXITED(status) && WEXITSTATUS(status) != 0) {
        printf("ps %d exited with %d\n", pid, WEXITSTATUS(status));
    }
    if(WIFSIGNALED(status)) {
        printf("ps %d killed with signal %d\n", pid, WTERMSIG(status));
    }
}
