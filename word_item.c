#include "word_item.h"
#include <stdio.h>
#include <stdlib.h>

void word_item_add(
        word_item **ll,
        union word_item_type_union data,
        enum word_item_type_enum type
    )
{
    word_item *new_node;
    new_node = malloc(sizeof(word_item));
    new_node->next = NULL;
    new_node->type = type;
    if(type == TYPE_WORD) {
        new_node->word = data.word;
    } else {
        new_node->tok = data.tok;
    }

    if(!*ll) {
        new_node->prev = NULL;
        *ll = new_node;
        return;
    }

    word_item *cur;
    cur = *ll;
    while(cur->next) {
        cur = cur->next;
    }
    cur->next = new_node;
    new_node->prev = cur;
}


void word_item_print(word_item *ll)
{
    while(ll) {
        if(ll->type == TYPE_WORD) {
            printf("%s -> ", ll->word);
        } else {
            printf("%s -> ", enum_tok_to_str(ll->tok));
        }
        ll = ll->next;
    }
    printf("\n");
}

void word_item_free(word_item *ll)
{
    while(ll) {
        word_item *tmp = ll;
        ll = ll->next;
        if(tmp->type == TYPE_WORD) {
            free(tmp->word);
        }
        free(tmp);
    }
}

int word_item_len(const word_item *ll)
{
    int len = 0;
    while(ll) {
        ll = ll->next;
        len++;
    }

    return len;
}

char **word_item_to_ntarr(const word_item *ll)
{
    /* converts linked list to NULL terminated array */

    int len = word_item_len(ll);
    char **res = malloc(sizeof(char*) * (len + 1));

    for(int i = 0; i < len; i++) {
        res[i] = ll->word;
        ll = ll->next;
    }
    res[len] = NULL;

    return res;
}
