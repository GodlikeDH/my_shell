#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "tokenizer.h"
#include "my_errors.h"

enum {INPUT_STR_LEN = 200};

int readline_shell(char **ret)
{
    /* This function reads one line from user input
     * and returns it as string
     * Also it handles EOF and exits program */

    char c;
    char *line;
    line = malloc(sizeof(char) * INPUT_STR_LEN);
    int size = 0;
    int capacity = INPUT_STR_LEN;

    printf("> ");
    while((c = getchar()) != '\n') {
        if(c == EOF) {
            printf("\n");
            exit(0);
        }
        if(size < capacity - 1) {
            line[size] = c;
            size++;
        } else {
            line = realloc(line, capacity * 2);
            capacity *= 2;
            line[size] = c;
            size++;
        }
    }
    line[size] = 0;

    *ret = line;

    return 0;
}

static int is_whitespace(char c)
{
    return c == ' ' || c == '\t';
}

static int is_delimeter(char c)
{
    return c == '&' || c == '|' ||
           c == '>' || c == '<' ||
           c == '(' || c == ')' ||
           c == ';';
}

static const char* find_quote_end(const char *str)
{
    char prev = '"';
    while(*str && (*str != '"' || prev == '\\')) {
        prev = *str;
        str++;
    }
    return str;
}

static int consume_word(const char *str, const char **ret)
{
    char prev = 0;
    while(1) {
        if((is_delimeter(*str) || *str == '\\') && prev == '\\') {
            prev = 0;
            str++;
        }
        if(prev == '\\' && *str != '"' &&
           !is_delimeter(*str) && *str != '\\') {
            return ERR_INP_SINGLE_ESCAPE;
        }

        if(!*str || is_whitespace(*str) || is_delimeter(*str)) {
            *ret = str;
            return 0;
        }
        if(*str == '"' && prev != '\\') {
            str++;
            str = find_quote_end(str);
            if(!*str) {
                return ERR_INP_UNMTCHD_QUOTES;
            }
        }
        prev = *str;
        str++;
    }
}

static const char* greedy_consume_delimeter(const char *str, char delim)
{
    str++;
    if(*str == delim) {
        str++;
    }
    return str;
}

static int consume_delimeter(const char *str, const char** ret, char delim)
{
    switch(delim) {
        case '>':
            *ret = greedy_consume_delimeter(str, delim);
            return 0;
        case '|':
        case '<':
        case '&':
            str++;
            *ret = str;
            return 0;
        default:
            return ERR_NOT_IMPLEMENTED;
    }
}

static char* create_token(const char* left, const char* right)
{
    char *token, *res;
    token = malloc(sizeof(char) * (right - left + 1));
    res = token;
    while(left < right) {
        if(*left == '\\') {
            left++;
            *token = *left;
            token++;
            left++;
            continue;
        }
        if(*left != '"') {
            *token = *left;
            token++;
        }
        left++;
    }

    *token = 0;

    return res;
}

int tokenize(const char* str, word_item **ret)
{
    /* Tokenizes input string into list of tokens */

    word_item *res = NULL;

    while(*str) {
        while(is_whitespace(*str)) {
            str++;
        }
        if(!*str) {
            *ret = res;
            return 0;
        }

        const char* word_start = str;
        int rc;
        enum word_item_type_enum token_type;

        if(is_delimeter(*str)) {
            rc = consume_delimeter(str, &str, *str);
            token_type = TYPE_DELIM;
        } else {
            rc = consume_word(str, &str);
            token_type = TYPE_WORD;
        }

        if(rc != SUCCESS) {
            *ret = res;
            return rc;
        }

        char *token = create_token(word_start, str);
        union word_item_type_union data;
        if(strlen(token) == 0) {    /* special case - empty token */
            data.tok = TOK_EMPTY;
            token_type = TYPE_DELIM;
        }

        if(token_type == TYPE_WORD) {
            data.word = token;
            word_item_add(&res, data, TYPE_WORD);
        } else {
            data.tok = str_tok_to_enum(token);
            word_item_add(&res, data, TYPE_DELIM);
        }
    }

    *ret = res;
    return 0;
}
