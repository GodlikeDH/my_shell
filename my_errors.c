#include "my_errors.h"

const char *error_strings[] = {
    "Success",
    "Error: Unmatched quotes\n",
    "Error: Single escape character '\\'\n",
    "Error: cd too many arguments\n",
    "Error: cd no home\n",
    "Error: command returned non-zero code\n",
    "Error: not implemented\n",
    "Error: cannot redirect input, wrong filename\n",
    "Error: cannot redurect input twice\n",
    "Error: cannot redirect output, wrong filename\n",
    "Error: cannot redurect output twice\n",
    "Error: wrong background token usage, & must be last token\n",
    "Error: command can't start with delimeter token\n",
    "Error: bad pipline usage (|)\n",
    "Error: two delimeter symbols next to each other\n",
};

