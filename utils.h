#ifndef UTILS_H_SENTRY
#define UTILS_H_SENTRY

int safe_close(int fd);
void print_exit_status(int status, int pid);

#endif
