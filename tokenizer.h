#ifndef TOKENIZER_H_SENTRY
#define TOKENIZER_H_SENTRY

#include "word_item.h"

int readline_shell(char **ret);
int tokenize(const char* str, word_item **ret);

#endif

