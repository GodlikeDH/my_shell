#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>

#include "command.h"
#include "my_errors.h"
#include "tokenizer.h"
#include "word_item.h"
#include "executor.h"
#include "signals.h"

void init_shell()
{
    const char hello_msg[] =
    "*******************************\n"
    "*******************************\n"
    "******* Welcome to shell ******\n"
    "*******************************\n"
    "*******************************\n";

    printf(hello_msg);

    signal(SIGCHLD, hangle_sigchld);
}

void print_in_brackets(word_item *tokens)
{
    while(tokens) {
        if(tokens->type == TYPE_WORD) {
            printf("[%s]\n", tokens->word);
        } else {
            printf("[%s]\n", enum_tok_to_str(tokens->tok));
        }
        tokens = tokens->next;
    }
}

void show_error(int return_code)
{
    if(return_code == -1) {
        perror("err");
        return;
    }

    printf("%s", error_strings[return_code]);
}

int main()
{
    init_shell();
    while(1) {
        char *input_str = NULL;
        int rc;
        rc = readline_shell(&input_str);

        word_item *tokens = NULL;
        rc = tokenize(input_str, &tokens);
        if(rc != SUCCESS) {
            show_error(rc);
            goto free_res;
        }

#ifdef DEBUG
        print_in_brackets(tokens);
        printf("-----------------------\n");
        commands_arr arr;
        rc = parse_commands(tokens, &arr);
        if(rc != SUCCESS) {
            show_error(rc);
        } else {
            print_commands(&arr);
        }
        printf("\n-----------------------\n");
        /*printf("-----------------------\n");*/
        printf("\n");

        goto free_res;
#endif

        rc = execute(tokens);
        if(rc != SUCCESS) {
            show_error(rc);
            goto free_res;
        }

free_res:
        free(input_str);
        word_item_free(tokens);
    }

    return 0;
}
