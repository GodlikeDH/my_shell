#include <signal.h>
#include <sys/wait.h>
#include <unistd.h>

#include "signals.h"
#include "utils.h"

static void wait_zombies()
{
    pid_t wpid;
    int status;
    do {
       wpid = waitpid(0, &status, WNOHANG);
       print_exit_status(status, wpid);
    } while (wpid > 0);
}

void hangle_sigchld(int s)
{
    signal(SIGCHLD, SIG_DFL); /* dont want to interrupt normal waits */
    wait_zombies();
    signal(SIGCHLD, hangle_sigchld);
}
